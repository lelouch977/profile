$('document').ready(function(){
   var isMobile = false; //initiate as false
   // device detection
   if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent) 
    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;

   $('.menu-button').click(function(){
      $(this).toggleClass('expanded')
   })

   $(window).scroll(function(){
      if ($(this).scrollTop() > 200) {
         $('.branding').addClass('small')
      } else if ($(this).scrollTop() < 150) {
         $('.branding').removeClass('small')
      }
   })

   $('.scroller',).click(function(e){
      e.preventDefault()
      var id = $(this)[0].target
      var offset = -30
      if (isMobile) {
         offset = -270
         $('.navbar-toggler').click()
      }
      $('html, body').animate({
        scrollTop: $(`.${id}`).offset().top + offset
    }, 700);
   })

   $.fn.preload = function (callback) {
     var length = this.length;
     var iterator = 0;

     return this.each(function () {
       var self = this;
       var tmp = new Image();

       if (callback) tmp.onload = function () {
         callback.call(self, 100 * ++iterator / length, iterator === length);
       };

       tmp.src = this.src;
     });
   };

   // if (!isMobile) {
   //    $('.imageToPreload').preload(function(perc, done) {
   //      $('.spinner').addClass('hide_preloader')
   //      $('.preloader_text').addClass('hide_preloader')
   //      setTimeout(function(){
   //       $('.preloader').addClass('hide_preloader')
   //      }, 500)
   //    });
   // } else {
   //    $('.preloader').addClass('hide_preloader')
   // }

   jQuery.easing['jswing'] = jQuery.easing['swing'];

   jQuery.extend( jQuery.easing,
   {
      def: 'easeOutQuad',
      swing: function (x, t, b, c, d) {
         //alert(jQuery.easing.default);
         return jQuery.easing[jQuery.easing.def](x, t, b, c, d);
      },
      easeInQuad: function (x, t, b, c, d) {
         return c*(t/=d)*t + b;
      },
      easeOutQuad: function (x, t, b, c, d) {
         return -c *(t/=d)*(t-2) + b;
      },
      easeInOutQuad: function (x, t, b, c, d) {
         if ((t/=d/2) < 1) return c/2*t*t + b;
         return -c/2 * ((--t)*(t-2) - 1) + b;
      },
      easeInCubic: function (x, t, b, c, d) {
         return c*(t/=d)*t*t + b;
      },
      easeOutCubic: function (x, t, b, c, d) {
         return c*((t=t/d-1)*t*t + 1) + b;
      },
      easeInOutCubic: function (x, t, b, c, d) {
         if ((t/=d/2) < 1) return c/2*t*t*t + b;
         return c/2*((t-=2)*t*t + 2) + b;
      },
      easeInQuart: function (x, t, b, c, d) {
         return c*(t/=d)*t*t*t + b;
      },
      easeOutQuart: function (x, t, b, c, d) {
         return -c * ((t=t/d-1)*t*t*t - 1) + b;
      },
      easeInOutQuart: function (x, t, b, c, d) {
         if ((t/=d/2) < 1) return c/2*t*t*t*t + b;
         return -c/2 * ((t-=2)*t*t*t - 2) + b;
      },
      easeInQuint: function (x, t, b, c, d) {
         return c*(t/=d)*t*t*t*t + b;
      },
      easeOutQuint: function (x, t, b, c, d) {
         return c*((t=t/d-1)*t*t*t*t + 1) + b;
      },
      easeInOutQuint: function (x, t, b, c, d) {
         if ((t/=d/2) < 1) return c/2*t*t*t*t*t + b;
         return c/2*((t-=2)*t*t*t*t + 2) + b;
      },
      easeInSine: function (x, t, b, c, d) {
         return -c * Math.cos(t/d * (Math.PI/2)) + c + b;
      },
      easeOutSine: function (x, t, b, c, d) {
         return c * Math.sin(t/d * (Math.PI/2)) + b;
      },
      easeInOutSine: function (x, t, b, c, d) {
         return -c/2 * (Math.cos(Math.PI*t/d) - 1) + b;
      },
      easeInExpo: function (x, t, b, c, d) {
         return (t==0) ? b : c * Math.pow(2, 10 * (t/d - 1)) + b;
      },
      easeOutExpo: function (x, t, b, c, d) {
         return (t==d) ? b+c : c * (-Math.pow(2, -10 * t/d) + 1) + b;
      },
      easeInOutExpo: function (x, t, b, c, d) {
         if (t==0) return b;
         if (t==d) return b+c;
         if ((t/=d/2) < 1) return c/2 * Math.pow(2, 10 * (t - 1)) + b;
         return c/2 * (-Math.pow(2, -10 * --t) + 2) + b;
      },
      easeInCirc: function (x, t, b, c, d) {
         return -c * (Math.sqrt(1 - (t/=d)*t) - 1) + b;
      },
      easeOutCirc: function (x, t, b, c, d) {
         return c * Math.sqrt(1 - (t=t/d-1)*t) + b;
      },
      easeInOutCirc: function (x, t, b, c, d) {
         if ((t/=d/2) < 1) return -c/2 * (Math.sqrt(1 - t*t) - 1) + b;
         return c/2 * (Math.sqrt(1 - (t-=2)*t) + 1) + b;
      },
      easeInElastic: function (x, t, b, c, d) {
         var s=1.70158;var p=0;var a=c;
         if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
         if (a < Math.abs(c)) { a=c; var s=p/4; }
         else var s = p/(2*Math.PI) * Math.asin (c/a);
         return -(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
      },
      easeOutElastic: function (x, t, b, c, d) {
         var s=1.70158;var p=0;var a=c;
         if (t==0) return b;  if ((t/=d)==1) return b+c;  if (!p) p=d*.3;
         if (a < Math.abs(c)) { a=c; var s=p/4; }
         else var s = p/(2*Math.PI) * Math.asin (c/a);
         return a*Math.pow(2,-10*t) * Math.sin( (t*d-s)*(2*Math.PI)/p ) + c + b;
      },
      easeInOutElastic: function (x, t, b, c, d) {
         var s=1.70158;var p=0;var a=c;
         if (t==0) return b;  if ((t/=d/2)==2) return b+c;  if (!p) p=d*(.3*1.5);
         if (a < Math.abs(c)) { a=c; var s=p/4; }
         else var s = p/(2*Math.PI) * Math.asin (c/a);
         if (t < 1) return -.5*(a*Math.pow(2,10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )) + b;
         return a*Math.pow(2,-10*(t-=1)) * Math.sin( (t*d-s)*(2*Math.PI)/p )*.5 + c + b;
      },
      easeInBack: function (x, t, b, c, d, s) {
         if (s == undefined) s = 1.70158;
         return c*(t/=d)*t*((s+1)*t - s) + b;
      },
      easeOutBack: function (x, t, b, c, d, s) {
         if (s == undefined) s = 1.70158;
         return c*((t=t/d-1)*t*((s+1)*t + s) + 1) + b;
      },
      easeInOutBack: function (x, t, b, c, d, s) {
         if (s == undefined) s = 1.70158; 
         if ((t/=d/2) < 1) return c/2*(t*t*(((s*=(1.525))+1)*t - s)) + b;
         return c/2*((t-=2)*t*(((s*=(1.525))+1)*t + s) + 2) + b;
      },
      easeInBounce: function (x, t, b, c, d) {
         return c - jQuery.easing.easeOutBounce (x, d-t, 0, c, d) + b;
      },
      easeOutBounce: function (x, t, b, c, d) {
         if ((t/=d) < (1/2.75)) {
            return c*(7.5625*t*t) + b;
         } else if (t < (2/2.75)) {
            return c*(7.5625*(t-=(1.5/2.75))*t + .75) + b;
         } else if (t < (2.5/2.75)) {
            return c*(7.5625*(t-=(2.25/2.75))*t + .9375) + b;
         } else {
            return c*(7.5625*(t-=(2.625/2.75))*t + .984375) + b;
         }
      },
      easeInOutBounce: function (x, t, b, c, d) {
         if (t < d/2) return jQuery.easing.easeInBounce (x, t*2, 0, c, d) * .5 + b;
         return jQuery.easing.easeOutBounce (x, t*2-d, 0, c, d) * .5 + c*.5 + b;
      }
   });

   function animateFace() {
      var designerImg   = $('#designer_img');
      var coderImg      = $('#coder_img');
      var designerHover = $('#designer');
      var coderHover    = $('#coder');
      var designerDesc  = $('#designer_desc');
      var coderDesc     = $('#coder_desc');      
      var designerBg    = $('#designer_bg');
      var coderBg       = $('#coder_bg');
      var face          = $('#face');
      var section       = $('#section__face');
      var duration      = 500;
      var loop = null;

      var mouseX = 0;
      var relMouseX = 520;
      var xp = 520;
      frameRate =  30;
      timeInterval = Math.round( 1000 / frameRate );     

      section.mouseenter(function(e){

         // Get mouse position
         section.mousemove(function(e){

            // raw mouse position
            mouseX = e.pageX;

            // mouse position relative to face div
            relMouseX = mouseX - face.offset().left;

         });
         
         // Animate the face based on mouse movement
         loop = setInterval(function(){

            // zeno's paradox dampens the movement
            xp += (relMouseX - xp) / 12;

            designerImg.css({width:420 + (520 - xp) * 0.5, left: 100 + (520 - xp) * 0.1});
            coderImg.css({width:420 + (xp - 520) * 0.5, right: 100 - (520 - xp) * 0.1});

            designerBg.css({left: 100 + (520 - xp) * 0.05, opacity: ((1040 - xp)/520)});
            coderBg.css({right:  100 + (xp - 520) * 0.05, opacity: (xp/520)});

            designerDesc.css({opacity: ((1040 - xp)/520)});
            coderDesc.css({opacity: (xp/520)});

         }, timeInterval );

      }).mouseleave(function(e){ 

         // reset the face to initial state
         clearInterval(loop);
         xp          = 520;
         mouseX      = 0;
         relMouseX   = 520;

         designerImg.hoverFlow(e.type, {width: 420, left: 100}, duration, 'easeOutQuad');
         coderImg.hoverFlow(e.type, {width: 420, right: 100}, duration, 'easeOutQuad');
         coderDesc.hoverFlow(e.type, {opacity: 1}, duration, 'easeOutQuad');
         designerDesc.hoverFlow(e.type, {opacity: 1}, duration, 'easeOutQuad');
         coderBg.hoverFlow(e.type, {right:100, opacity: 1}, duration, 'easeOutQuad');
         designerBg.hoverFlow(e.type, {left:100, opacity: 1}, duration, 'easeOutQuad');

      }); 
      
   };

   animateFace();
   $('.tech_stack').owlCarousel({
      items: 4,
      loop: true,
      autoplay: true,
      autoplayTimeout: 5000,
      dots: true,
      responsive: {
         0: {
            items: 1
         },
         768: {
            items: 2
         },
         992: {
            items: 3
         }
      }
   });
   new WOW().init({
      mobile: false
   });
})

function initMap() {
  var balay = {lat: 10.4296931, lng: 125.0314419};
  var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 9,
    center: balay
  });
  var marker = new google.maps.Marker({
    position: balay,
    map: map
  });
}